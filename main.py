# Run this app with `python app.py` and
# visit http://127.0.0.1:8050/ in your web browser.

import dash
import dash_core_components as dcc
import dash_html_components as html
import plotly.express as px
import pandas as pd

app = dash.Dash(__name__)
server = app.server

flights = pd.read_csv("flights.csv.gz")
airports = pd.read_csv("airports.csv")

joined = flights.set_index('dest').join(airports.set_index('faa'))
gb = joined.groupby(by=['state'])
avgdd = gb.mean()['dep_delay']
avgdd = pd.DataFrame({'state': avgdd.index, 'time': avgdd.values})
avgad = gb.mean()['arr_delay']
avgad = pd.DataFrame({'state': avgad.index, 'time': avgad.values})

fig1 = px.choropleth(avgdd, locations='state', locationmode='USA-states',
                     color='time', scope='usa', labels={'time': 'Average depart delay time'})

fig2 = px.choropleth(avgad, locations='state', locationmode='USA-states',
                     color='time', scope='usa', labels={'time': 'Average arrive delay time'})

app.layout = html.Div(children=[
    html.H1(children='Average Delay time'),

    html.Div(children='''
        Average depart delay time graph
    '''),

    dcc.Graph(
        id='example-graph',
        figure=fig1
    ),

    html.Div(children='''
        Average arrive delay time graph
    '''),

    dcc.Graph(
        id='example-graph2',
        figure=fig2
    ),

    html.Div(children='''
        We can see that these two average delay time are positive correlated. States like OK have both short(yellow) average depart delay time and average arrive delay time.
    ''')
])

if __name__ == '__main__':
    app.run_server(debug=True, port=8080)
